FROM golang:1.21 AS build-env
WORKDIR /go-src
COPY go.mod /
#COPY go.sum /
RUN go mod download
ADD . /go-src
RUN CGO_ENABLED=0 go build -o /go-service

FROM scratch
COPY --from=build-env /go-service /go-service
COPY templates templates/
ENTRYPOINT ["/go-service"]
