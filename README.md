# go-service

This is a simple containerized Go service; when deployed, it displays the
message found in [templates/home.html](templates/home.html).

The [.gitlab-ci.yml](.gitlab-ci.yml) demonstrates an end-to-end CICD Pipeline
using GitLab Components to:

- build a container
- push the container to Google Artifact Registry
- deploy the container to Google Kubernetes Engine
- deploy the container to Cloud Run
- upload a copy of this repository to GCS
